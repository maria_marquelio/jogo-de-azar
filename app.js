btnJogar = document.getElementById("btnJogar"); 
btnJogar.addEventListener("click", jogar);

btnReset = document.getElementById("btnReset"); 
btnReset.addEventListener("click", zerarFichas);

destino = document.getElementById("imagens_geradas");
resultado = document.getElementById("resultado");

let fichas = 0; 

function jogar(){

    limparTela(); 

    let img1 = gerarNum(); 
    let img2 = gerarNum(); 
    let img3 = gerarNum(); 

    for (let i=1; i <= 8; i++){

        if(img1==i){
            let img_1 = document.createElement("img");
            img_1.className = "imagem";
            img_1.src = ( 'img/' + i + ".jpg");         
            destino.appendChild(img_1); 
        }
        if(img2==i){
            let img_2 = document.createElement("img");
            img_2.className = "imagem";
            img_2.src = ('img/' + i + ".jpg");         
            destino.appendChild(img_2); 
        }
        if(img3==i){
            let img_3 = document.createElement("img");
            img_3.className = "imagem";
            img_3.src = ('img/' + i + ".jpg");         
            destino.appendChild(img_3); 
        }
    }

    if (img1===img2 && img1 === img3){
        let result = document.createElement("span");
        result = document.createTextNode("Parabêns ... Você ganhou 100 fichas   |  Saldo:" + (fichas=fichas + 100) + " fichas. ");
        resultado.appendChild(result); 
    } 

    else {
        let result = document.createElement("span");
        result = document.createTextNode("Infelizmente você não ganhou fichas...     |  Saldo: " + (fichas) + " fichas. ");
        resultado.appendChild(result); 

    }
}

function gerarNum(){
    return Math.floor(Math.random() * 8 + 1);    
}

function zerarFichas(){
    
    limparTela(); 

    if (fichas === 0){
        result = document.createTextNode("Infelizmente você não possui fichas a ser zeradas ...  ");   
    } 
    else {
        fichas = 0; 
        result = document.createTextNode("Seu saldo de fichas foi zerado... "); 
    }

    resultado.appendChild(result);
}

function limparTela(){
    destino.innerHTML = " ";
    resultado.innerHTML = " ";
}